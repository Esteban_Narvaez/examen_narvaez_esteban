﻿using examen_Narvaez_Esteban.modelos;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xamarin.Forms;

namespace examen_Narvaez_Esteban
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(Object seder, EventArgs e)
        {
            login log = new login
            {
                correo = txtCorreo.Text,
                contr = txtContr.Text
            };


            Uri RequesUri = new Uri("http://localhost:3000/api/login");
            
            //Declaracion de variable 
            var client = new HttpClient();

            //Convertir objeto a tipo json
            var json = JsonConvert.SerializeObject(log);

            var contentJson = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(RequesUri, contentJson);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                await Navigation.PushAsync(new Inicio());
            }
            else
            {
                await DisplayAlert("Mensaje", "Datos Invalidos", "OK");
            }
        }
    }
}
